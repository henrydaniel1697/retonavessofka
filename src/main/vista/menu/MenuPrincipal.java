package main.vista.menu;

import main.modelo.menu.*;
import main.modelo.naves.Naves;
import main.vista.naves.AgregarNave;
import main.vista.naves.InventarioNavesAvanzado;
import main.vista.naves.InventarioNavesSimple;
import main.vista.naves.Simulacro;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;

//Aqui se ubica el main.modelo.menu principal
public class MenuPrincipal extends EstructuraMenu {

    InventarioNavesSimple inventarioNavesSimple = new InventarioNavesSimple();
    InventarioNavesAvanzado inventarioNavesAvanzado = new InventarioNavesAvanzado();
    AgregarNave agregarNave = new AgregarNave();
    //Se utiliza un array list para guardar las main.modelo.naves y las que se creen.
    ArrayList<Naves> listaNaves = new ArrayList<>(Arrays.asList(saturnoV, energia, arianeV, explorer, sputnik, soho, skylab, salyut, shenzou));

    Simulacro simulacro = new Simulacro();
    @Override
    public void ingresarMenu() {

        int opcion = 0;

        while (opcion != 5) {
            //Aqui se ingresa a las diferentes funcionalidades
            try {

                opcion = Integer.parseInt(JOptionPane.showInputDialog("""
                        Bienvenido al sistema de archivos de la NASA
                        Por favor, selecciona una opción
                        1.- Añadir nave.
                        2.- Busqueda inventario simple.
                        3.- Busqueda inventario avanzado.
                        4.- Simulacro de vuelo.
                        5.- Salir."""));

                switch (opcion) {
                    case 1 -> agregarNave.crearNave(listaNaves);
                    case 2 -> inventarioNavesSimple.ingresarMenu(listaNaves);
                    case 3 -> inventarioNavesAvanzado.ingresarMenu(listaNaves);
                    case 4 -> simulacro.ingresarMenu(listaNaves);
                }

                if (opcion < 0 || opcion > 5){

                    JOptionPane.showMessageDialog(null,"Por favor selecciona una opción valida");

                }
            // Agregamos un try catch para evitar errores
            }catch (Exception ex){
                JOptionPane.showMessageDialog(null,"Por favor selecciona una opción valida, devuelta al menu principal.");
            }

        }

        System.out.println("");
        System.out.println("¡Gracias por utilizar nuestro sistema!");
        System.out.println("");
        System.out.println("""
                  |* * * * * * * * * * OOOOOOOOOOOOOOOOOOOOOOOOO|
                  | * * * * * * * * *  OOOOOOOOOOOOOOOOOOOOOOOOO|
                  |* * * * * * * * * * OOOOOOOOOOOOOOOOOOOOOOOOO|
                  | * * * * * * * * *  OOOOOOOOOOOOOOOOOOOOOOOOO|
                  |* * * * * * * * * * OOOOOOOOOOOOOOOOOOOOOOOOO|
                  | * * * * * * * * *  OOOOOOOOOOOOOOOOOOOOOOOOO|
                  |* * * * * * * * * * OOOOOOOOOOOOOOOOOOOOOOOOO|
                  |OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO|
                  |OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO|
                  |OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO|
                  |OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO|
                  |OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO|
                  |OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO|\
                """);

    }
}
