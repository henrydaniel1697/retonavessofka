package main.vista.naves;

import main.modelo.naves.Naves;

import javax.swing.*;
import java.util.ArrayList;

//Clase para la busqueda de main.modelo.naves simple, solo se busca por tipo de nave, se muestran todas las relacionadas.
public class InventarioNavesSimple {

    public void ingresarMenu(ArrayList<Naves> listaNaves) {

        Naves resultado;

        int opcionInventario;

        opcionInventario = Integer.parseInt(JOptionPane.showInputDialog("""
                ¿Qué tipo de nave buscas?
                1.- No tripulada
                2.- Tripulada
                3.- Vehiculo Lanzadera"""));

        switch (opcionInventario) {
            case 1 -> {
                for (Naves naves : listaNaves) {
                    resultado = naves;
                    if (resultado.getTipoDeNave().equals("Nave No Tripulada")) {
                        JOptionPane.showMessageDialog(null, "Nombre: " + resultado.getNombre() +
                                "\nTipo de nave: " + resultado.getTipoDeNave() +
                                "\nAltura: " + resultado.getAltura() + " m" +
                                "\nPeso: " + resultado.getPeso() +
                                " ton" + "\nPais: " + resultado.getPais());
                    }
                }
                JOptionPane.showMessageDialog(null, "Busqueda exitosa, vuelves al menu.");
            }
            case 2 -> {
                for (Naves naves : listaNaves) {
                    resultado = naves;
                    if (resultado.getTipoDeNave().equals("Nave tripulada")) {
                        JOptionPane.showMessageDialog(null, "Nombre: " + resultado.getNombre() +
                                "\nTipo de nave: " + resultado.getTipoDeNave() +
                                "\nAltura: " + resultado.getAltura() + " m" +
                                "\nPeso: " + resultado.getPeso() +
                                " ton" + "\nPais: " + resultado.getPais());
                    }
                }
                JOptionPane.showMessageDialog(null, "Busqueda exitosa, vuelves al menu.");
            }
            case 3 -> {
                for (Naves naves : listaNaves) {
                    resultado = naves;
                    if (resultado.getTipoDeNave().equals("Vehiculo Lanzadera")) {
                        JOptionPane.showMessageDialog(null, "Nombre: " + resultado.getNombre() +
                                "\nTipo de nave: " + resultado.getTipoDeNave() +
                                "\nAltura: " + resultado.getAltura() + " m" +
                                "\nPeso: " + resultado.getPeso() +
                                " ton" + "\nPais: " + resultado.getPais());
                    }


                }
                JOptionPane.showMessageDialog(null, "Busqueda exitosa, vuelves al menu.");
            }
        }

                if (opcionInventario <= 0 || opcionInventario >= 4) {
                    JOptionPane.showMessageDialog(null, "Opción incorrecta, devuelta al menu principal");
                }

        }

    }

