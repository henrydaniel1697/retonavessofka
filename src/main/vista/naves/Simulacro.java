package main.vista.naves;

import main.modelo.naves.Naves;
import main.modelo.naves.NoTripuladas;
import main.modelo.naves.Tripuladas;
import main.modelo.naves.VehiculosLanzadera;

import javax.swing.*;
import java.util.ArrayList;
//Clase para simular las acciones de las main.modelo.naves
public class Simulacro{

    protected VehiculosLanzadera accionesVL = new VehiculosLanzadera("", "", 0, 0,0, "",21);

    protected NoTripuladas accionesNT = new NoTripuladas("", "", 0, 0, "");

    protected Tripuladas accionesT = new Tripuladas("", "", 0, 0, 0, "");

    public void ingresarMenu(ArrayList<Naves> listaNaves){

        Naves resultado;

        String nombreDeNave;

        System.out.println("Lista de main.modelo.naves: ");
                for (Naves naves : listaNaves) {
                    resultado = naves;
                    System.out.println("- " + resultado.getNombre());
                }

                nombreDeNave = JOptionPane.showInputDialog(null,"Escribe el nombre de la nave que deseas utilizar ");

                for (Naves naves : listaNaves) {

                    if (naves.getNombre().toLowerCase().contains(nombreDeNave.toLowerCase()) && naves.getTipoDeNave().equals("Nave No Tripulada")) {

                       JOptionPane.showMessageDialog(null,"Nave utilizada: " + naves.getNombre());
                       accionesNT.despegar();
                       accionesNT.entrarEnOrbita(50);
                       break;

                    } else if (naves.getNombre().toLowerCase().contains(nombreDeNave.toLowerCase()) && naves.getTipoDeNave().equals("Nave tripulada")) {

                        JOptionPane.showMessageDialog(null,"Nave utilizada: " + naves.getNombre());
                        accionesT.despegar();
                        accionesT.aterrizar();
                        break;

                    } else if (naves.getNombre().toLowerCase().contains(nombreDeNave.toLowerCase()) && naves.getTipoDeNave().equals("Vehiculo Lanzadera")){

                        JOptionPane.showMessageDialog(null,"Nave utilizada: " + naves.getNombre());
                        accionesVL.despegar();
                        accionesVL.entrarEnOrbita(100);
                        break;

                    }

                }

        JOptionPane.showMessageDialog(null,"Simulacro terminado. Gracias por participar.");

    }
}
