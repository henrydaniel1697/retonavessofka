package main.modelo.naves;

//Clase padre para las main.modelo.naves
public class Naves {

    //Los elementos generales de las naves
    protected String nombre;

    protected String tipoDeNave;
    protected double altura;
    protected double peso;
    protected String pais;

    //Sus getters y setters
    public String getTipoDeNave() {
        return tipoDeNave;
    }

    public void setTipoDeNave(String tipoDeNave) {
        this.tipoDeNave = tipoDeNave;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String despegar() {
        return null;
    }

    //Metodos generales
    public String entrarEnOrbita(){
        return null;
    }

    protected String aterrizar(){
        return null;
    }

    //Constructor
    public Naves(String nombre, String tipoDeNave, double altura, double peso, String pais) {
        this.nombre = nombre;
        this.tipoDeNave = tipoDeNave;
        this.altura = altura;
        this.peso = peso;
        this.pais = pais;
    }
}
