package main.modelo.menu;

import main.modelo.naves.NoTripuladas;
import main.modelo.naves.Tripuladas;
import main.modelo.naves.VehiculosLanzadera;

//Clase abstracta para el main.modelo.menu, se coloca la información recibida de la documentación de las main.modelo.naves
public abstract class EstructuraMenu {

    protected VehiculosLanzadera saturnoV = new VehiculosLanzadera("SaturnoV", "Vehiculo Lanzadera", 32000, 100,2900, "EEUU (Estados Unidos)",118);
    protected VehiculosLanzadera energia = new VehiculosLanzadera("Energia", "Vehiculo Lanzadera", 32000, 60,2400, "Rusia",100);
    protected VehiculosLanzadera arianeV = new VehiculosLanzadera("ArianeV", "Vehiculo Lanzadera", 8676, 59,777, "Europa",21);

    protected NoTripuladas explorer = new NoTripuladas("Explorer", "Nave No Tripulada",  2, 0.013, "EEUU (Estados Unidos)");
    protected NoTripuladas sputnik = new NoTripuladas("Sputnik", "Nave No Tripulada", 2, 0.083, "Rusia");
    protected NoTripuladas soho = new NoTripuladas("Soho", "Nave No Tripulada", 4.3, 1.8, "Europa");
    protected Tripuladas skylab = new Tripuladas("Skylab", "Nave tripulada", 12400, 11.1, 75, "EEUU (Estados Unidos)");
    protected Tripuladas salyut = new Tripuladas("Salyut", "Nave tripulada", 4500, 15.8, 19, "Rusia");
    protected Tripuladas shenzou = new Tripuladas("Shenzou", "Nave tripulada", 8000, 9.25, 7.8, "China");
    //metodo abtracto
    public abstract void ingresarMenu();

}
