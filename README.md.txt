Saludos,

Este proyecto se realiza con el objetivo de la realización del ejercicio para el reto SofkaU para el Training de Automatización, este se basa en el siguiente link (https://moaramore.com/2016/05/14/clasificacion-de-las-naves-espaciales).

El programa cuenta con una ejecución infinita, en el cual puede usar las opciones de "Añadir nave" , "Búsqueda inventario simple", "Búsqueda inventario avanzado", "Simulacro de vuelo" y su respectiva finalización del programa si lo desea el usuario.

A continuación se indica paso a paso el uso proyecto:

- Instrucciones:

Una vez ingresado a la aplicación desde el IDE, es necesario desplegar la carpeta "src" permitiendo ver tanto la carpeta "main" y "test".
En el proyecto se encuentra el package "src.main.controlador", desplegamos este.
Lo ejecutamos con la clase "Main".
Dependiendo de el IDE usada, podrá ser ejecutado con la función "play" o con el simbolo "►".

Para las pruebas unitarias.

Para realizar Test es necesario acceder en la carpeta Test y puede reproducirse con la función "play" o con el símbolo "►".